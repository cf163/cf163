#include<stdio.h>
int main()
{
    int i,search,beg,end,mid,flag,p,n,a[50];
    printf("enter the size of array");
    scanf("%d",&n);
    printf("enter the array elements");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("enter the element to be searched");
    scanf("%d",&search);
    flag=0;
    beg=0;
    end=n-1;
    while(beg<=end)
    {
        mid=(beg+end)/2;
        if(a[mid]==search)
        {
            p=mid;
            flag=1;
            break;
        }
        else if(a[mid]<search)
        {
            beg=mid+1;
        }
        else
        {
            end=mid-1;
        }
    }
    if(flag==1)
    {
        printf("searched element %d is found at position %d",search,p+1);
    }
    else
    {
        printf("sorry we couldnt find the searched element");
    }
    return 0;
}