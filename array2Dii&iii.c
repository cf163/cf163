#include<stdio.h>
int main()
{
    int i,j,x,y,a[50][50],b[50][50],c[50][50],d[50][50];
    printf("enter the number of rows and columns in the two matrices");
    scanf("%d %d",&x,&y);
    printf("matrix A details\n");
    for(i=1;i<=x;i++)
    {
        printf("enter row %d elements",i);
        for(j=1;j<=y;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    printf("matrix B details\n");
    for(i=1;i<=x;i++)
    {
        printf("enter row %d elements",i);
        for(j=1;j<=y;j++)
        {
            scanf("%d",&b[i][j]);
        }
    }
    for(i=1;i<=x;i++)
    {
        for(j=1;j<=y;j++)
        {
          c[i][j]=a[i][j]+b[i][j];
          d[i][j]=a[i][j]-b[i][j];
        }
    }
    printf("addition of matrices\n");
    for(i=1;i<=x;i++)
    {
        for(j=1;j<=y;j++)
        {
            printf("%d\t",c[i][j]);
        }
        printf("\n");
    }
    printf("difference of the matrices\n");
    for(i=1;i<=x;i++)
    {
        for(j=1;j<=y;j++)
        {
            printf("%d\t",d[i][j]);
        }
        printf("\n");
    }
    return 0;
}